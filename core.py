import urllib.request
import gzip
import io
import random
import time
import MySQLdb

host = 'localhost'
user = 'root'
password = '2505'
db_name = 'media'

class ScrapBase:
    def __init__(self, url, walker, parser):

        self.db = MySQLdb.connect(host, user, password, db_name)

        # base url of website
        self.base_url = url

        # delay specified in robots.txt
        self.robots_txt_delay = 0

        # generator of uris for specified website
        self.Walker = walker

        # function for parsing and extracting data for specified website
        self.ParseFunc = parser

        # id for base_url in table platform
        self.platform_id = -1
        self.get_platform_id()

        self.user_agents = list()

    def __del__(self):
        self.db.close()

    def sleep(self):
        if self.robots_txt_delay:
            time.sleep(self.robots_txt_delay)
        else:
            sleep_time = random.randint(60, 120)
            time.sleep(sleep_time)

    def get_url(self):
        return self.base_url

    def set_url(self, url):
        self.base_url = url

    def get_delay(self):
        return self.robots_txt_delay

    def set_delay(self, delay):
        self.robots_txt_delay = delay

    def get_html(self, url):
        print("Getting html for url: ", url)

        retry = 5
        while retry > 0:
            try:
                request = urllib.request.Request(url)

                length = len(self.user_agents)
                index = random.randint(0, length - 1)
                user_agent = self.user_agents[index]
                request.add_header('User-agent', user_agent)
                request.add_header('connection', 'keep-alive')
                request.add_header('Accept-Encoding', 'gzip')
                request.add_header('referer', self.base_url)

                response = urllib.request.urlopen(request)

                html = response.read()

                if response.headers.get('content-encoding', None) == 'gzip':
                    html = gzip.GzipFile(fileobj=io.BytesIO(html)).read()
                return html
                break

            except urllib.error.HTTPError as e:
                print('url error:', e)
                self.sleep()
                retry -= 1
                failed_links_file = open("failed_links.txt", 'a')
                failed_links_file.write(url)
                failed_links_file.close()
                continue

            except Exception as e:
                print("error:", e)
                retry -= 1
                self.sleep()
                failed_links_file = open("failed_links.txt", 'a')
                failed_links_file.write(url)
                failed_links_file.close()
                continue

    # info: list of dictionaries [{'url':'...', 'title':'...' , 'price': '...' , 'region': '...'}, ...]
    def update_db(self, info, platform_id):
        print("Updating DB with new data ...")
        try:
            with self.db.cursor() as cursor:
                for item in info:
                    url = item['url'].encode("utf-8")
                    title = item['title'].encode("utf-8")
                    if 'price' in item:
                        price = item['price'].encode("utf-8")
                    else:
                        price = None
                    if 'region' in item:
                        region = item['region'].encode("utf-8")
                    else:
                        region = None

                    # Create a new record
                    sql = "INSERT INTO `media_info` (`url`, `title`, `platform_id`) VALUES (%s, %s, %s) " \
                          "ON DUPLICATE KEY UPDATE creation_time = NOW()"
                    cursor.execute(sql, (url, title, platform_id))

            # # connection is not autocommit by default. So you must commit to save
            self.db.commit()
            print("Updated!")
        except Exception as e:
            self.db.rollback()
            print("error:", e)
            pass

    # ParseFunc - function for html parsing
    # returns dictionary: ['title':'...' , 'price': '...' , 'region': '...')
    def process(self):
        try:
            self.load_user_agent()
            urls = self.Walker
            for url in urls():
                print("Processing ", url, " ...")
                html = self.get_html(url)
                if not html:
                    print("Error. Got empty html for url ", url)
                else:
                    extracted_data = self.ParseFunc(html)
                    print("Html parsed successfully!")
                    self.update_db(extracted_data, self.platform_id)
                time.sleep(self.robots_txt_delay)
                print("\n")
        except Exception as e:
            print("error:", e)
            exit()

    def get_platform_id(self):
        try:
            with self.db.cursor() as cursor:
                sql = "SELECT id FROM platform WHERE url = '%s'" % self.base_url
                cursor.execute(sql)
                platform_id = cursor.fetchone()

                if not platform_id:
                    sql = "INSERT INTO platform(url) VALUES ('%s')" % self.base_url
                    cursor.execute(sql)
                    self.db.commit()
                    self.platform_id = cursor.lastrowid
                    print("Added new platform to ", db_name, " (", self.platform_id, ":", self.base_url, ")")
                else:
                    self.platform_id = platform_id[0]

        except Exception as e:
            print("error:", e)
            exit()

    def load_user_agent(self):
        fp = open('./user_agents.txt', 'r')
        line = fp.readline().strip('\n')

        while line:
            self.user_agents.append(line)
            line = fp.readline().strip('\n')

        fp.close()



