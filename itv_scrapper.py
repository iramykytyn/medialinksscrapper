from core import ScrapBase
from bs4 import BeautifulSoup


baseURL = 'http://www.itv.com'


def parse_func(html):
    soup = BeautifulSoup(html, 'html.parser')

    # list of dictionaries [{'url':'...', 'title':'...' }, ...]
    extracted_data = list()

    for tag in soup.find_all('li', attrs={'class': 'grid-list__item width--one-half width--custard--one-third js-lazy'}):
        for item in tag.find_all('a', attrs={'class': 'complex-link'}):
            media_dict = {}
            url = item['href']
            h3 = item.find('h3', attrs={'class': 'tout__title complex-link__target theme__target'})
            title = h3.getText()
            media_dict['url'] = (baseURL + url).strip()
            media_dict['title'] = title.strip()
            extracted_data.append(media_dict)

    return extracted_data


def walker():
    url = baseURL + '/hub/shows'
    yield url


def RunItvScrapper():
    print("ITV scrapper started ...")
    core_scrapper = ScrapBase(baseURL, walker, parse_func)
    core_scrapper.process()
    print("ITV scrapper finished successfully!\n")

