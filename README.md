# Short instruction #

### 
1. Before run scrapper you need to have: MySql server installed and started, Python3 with BeautifulSoup and MySQLdb libs installed
1. Replace 'root'@'localhost' in create_db.sql with your mysql server username and host
1. Run create_db.sql script to create database:

```
#!shell

mysql -u <user_name> -p <  create_db.sql

```
Specify credentials for access to your Mysql database in core.py.
Example:

```
#!python
host = 'localhost'
user = 'root'
password = '2505'
db_name = 'media'
```

Run scrapper: 

```
#!python

python run_scrapper.py

```
###

**Note! Do not forget to use python 3**