DROP DATABASE IF EXISTS media; CREATE DATABASE media; USE media;
GRANT ALL ON media TO 'root'@'localhost';


CREATE TABLE IF NOT EXISTS platform (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    url VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS media_info (
   id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
   url VARCHAR(250) NOT NULL UNIQUE,
   title VARCHAR(250) NOT NULL,
   creation_time TIMESTAMP DEFAULT NOW(),
   region VARCHAR(250) NULL,
   price VARCHAR(50) NULL,
   platform_id INT NOT NULL,
   FOREIGN KEY (platform_id) REFERENCES platform(id)
);

INSERT INTO platform(url)
VALUES ('http://www.bbc.co.uk');

INSERT INTO platform(url)
VALUES ('http://www.channel4.com');

INSERT INTO platform(url)
VALUES ('http://www.itv.com');