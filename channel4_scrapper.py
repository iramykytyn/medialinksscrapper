from core import ScrapBase
from bs4 import BeautifulSoup


baseURL = 'http://www.channel4.com'


def parse_func(html):
    soup = BeautifulSoup(html, 'html.parser')

    # list of dictionaries [{'url':'...', 'title':'...' }, ...]
    extracted_data = list()

    for tag in soup.find_all('h3', attrs={'class': 'azBlock-title'}):
        media_dict = {}
        item = tag.find('a', attrs={'class': 'link'})
        url = item['href']
        title = item.getText()
        media_dict['url'] = (baseURL + url).strip()
        media_dict['title'] = title.strip()
        extracted_data.append(media_dict)

    return extracted_data


def walker():
    url = baseURL + '/programmes/categories/all/'
    start_char_numb = ord('a')
    while start_char_numb <= ord('z'):
        yield url + chr(start_char_numb)
        start_char_numb += 1
        if start_char_numb > ord('z'):
            yield url + '0-9'


def RunChannel4Scrapper():
    print("Channel4 scrapper started ...")
    core_scrapper = ScrapBase(baseURL, walker, parse_func)
    core_scrapper.set_delay(5)
    core_scrapper.process()
    print("Channel4 scrapper finished successfully!\n")

