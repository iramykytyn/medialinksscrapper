from core import ScrapBase
from bs4 import BeautifulSoup


baseURL = 'http://www.bbc.co.uk'


def parse_func(html):
    soup = BeautifulSoup(html, 'html.parser')

    # list of dictionaries [{'url':'...', 'title':'...' }, ...]
    extracted_data = list()

    for tag in soup.find_all('ol', attrs={'class': 'tleo-list left'}):
        for item in tag.find_all('a'):
            media_dict = {}
            url = item['href']
            title = item.span.getText()
            media_dict['url'] = (baseURL + url).strip()
            media_dict['title'] = title.strip()
            extracted_data.append(media_dict)

    for tag in soup.find_all('ol', attrs={'class': 'tleo-list right'}):
        for item in tag.find_all('a'):
            media_dict = {}
            url = item['href']
            title = item.span.getText()
            media_dict['url'] = (baseURL + url).strip()
            media_dict['title'] = title.strip()
            extracted_data.append(media_dict)

    return extracted_data


def walker():
    url = baseURL + '/iplayer/a-z/'
    start_char_numb = ord('a')
    while start_char_numb <= ord('z'):
        yield url + chr(start_char_numb)
        start_char_numb += 1
        if start_char_numb > ord('z'):
            yield url + '0-9'


def RunBBCscrapper():
    print("BBC scrapper started ...")
    core_scrapper = ScrapBase(baseURL, walker, parse_func)
    core_scrapper.process()
    print("BBC scrapper finished successfully!\n")

