from bbc_scrapper import RunBBCscrapper
from itv_scrapper import RunItvScrapper
from channel4_scrapper import RunChannel4Scrapper

# ------------------------------------------------ Guid ---------------------------------------------
# 1. Before run scrapper you need to have:
#       a) MySql server installed and started,
#       b) Python3 with BeautifulSoup and MySQLdb libs
# 2. Replace 'root'@'localhost' in create_db.sql with your mysql server username and host
# 3. Run create_db.sql script to create database: mysql -u <user_name> -p <  create_db.sql
# 4. Specify credentials for access to your Mysql database in core.py (host, user, password, db_name)
# 5. Run scrapper: python3 run_scrapper.py

print("Scrappers started ...")
RunBBCscrapper()
RunItvScrapper()
RunChannel4Scrapper()
print("Scrapping finished.")